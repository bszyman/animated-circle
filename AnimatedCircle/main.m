//
//  main.m
//  AnimatedCircle
//
//  Created by Ben Szymanski on 12/8/15.
//  Copyright © 2015 Build Another Software Ltd. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
