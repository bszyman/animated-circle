//
//  AppDelegate.m
//  AnimatedCircle
//
//  Created by Ben Szymanski on 12/8/15.
//  Copyright © 2015 Ben Szymanski. All rights reserved.
//
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#import "AppDelegate.h"
#import <Quartz/Quartz.h>

@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;
@property (nonatomic, strong) CALayer *mainLayer;

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddEllipseInRect(path, NULL, NSMakeRect(25.0, 25.0, 150.0, 150.0));
    
    CAShapeLayer *firstShape = [CAShapeLayer layer];
    firstShape.frame = NSMakeRect(0, 0, 200, 200);
    firstShape.bounds = NSMakeRect(0, 0, 200, 200);
    firstShape.path = path;
    firstShape.fillColor = CGColorCreateGenericRGB(0.0, 0.0, 0.0, 1.0);
    //firstShape.position = CGPointMake(0.5, 0.5);
    firstShape.anchorPoint = CGPointMake(0.5, 0.5);
    [firstShape setNeedsDisplay];
    
    CAShapeLayer *secondShape = [CAShapeLayer layer];
    secondShape.frame = NSMakeRect(0, 0, 200, 200);
    secondShape.bounds = NSMakeRect(0, 0, 200, 200);
    secondShape.path = path;
    secondShape.fillColor = CGColorCreateGenericRGB(0.0, 0.0, 0.0, 1.0);
    //secondShape.position = CGPointMake(0.5, 0.5);
    firstShape.anchorPoint = CGPointMake(0.5, 0.5);
    [secondShape setNeedsDisplay];
    
    
    self.mainLayer = [CALayer layer];
    [self.mainLayer addSublayer:secondShape];
    [self.mainLayer addSublayer:firstShape];
    [self.mainLayer setNeedsDisplay];
    
    //[self.window.contentView.layer addSublayer:self.mainLayer];
    
    CFRelease(path);
    
}

//- (IBAction)doYouDo:(NSButton *)sender
//{
//    NSLog(@"doYouDo");
//
//    NSArray *layers = self.mainLayer.sublayers;
//
//    CAShapeLayer *layerToAnimate = [layers firstObject];
//    //layerToAnimate.contentsScale = 1.50;
//
//    CABasicAnimation *scaleAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
//    scaleAnimation.fromValue = [NSNumber numberWithFloat:1.0f];
//    scaleAnimation.toValue = [NSNumber numberWithFloat:1.20f];
//    scaleAnimation.duration = 0.30f;
//    scaleAnimation.fillMode = kCAFillModeForwards;
//    scaleAnimation.removedOnCompletion = NO;
//
//    CABasicAnimation *colorAnimation = [CABasicAnimation animationWithKeyPath:@"fillColor"];
//    colorAnimation.fromValue = (__bridge id _Nullable)(CGColorCreateGenericRGB(0.0, 0.0, 0.0, 1.0));
//    colorAnimation.toValue = (__bridge id _Nullable)(CGColorCreateGenericRGB(0.0, 1.0, 0.0, 1.0));
//    colorAnimation.duration = 0.15f;
//    colorAnimation.fillMode = kCAFillModeForwards;
//    colorAnimation.removedOnCompletion = NO;
//
//    CAAnimationGroup *animations = [CAAnimationGroup animation];
//    animations.duration = 0.30f;
//    animations.animations = @[scaleAnimation, colorAnimation];
//    animations.fillMode = kCAFillModeForwards;
//    animations.removedOnCompletion = NO;
//    animations.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
//
//
//    [layerToAnimate addAnimation:animations forKey:nil];
//}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

@end
