//
//  InteractiveView.m
//  AnimatedCircle
//
//  Created by Ben Szymanski on 12/9/15.
//  Copyright © 2015 Ben Szymanski. All rights reserved.
//
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#import "InteractiveView.h"
#import <QuartzCore/QuartzCore.h>

@interface InteractiveView ()

@property (nonatomic, strong) CALayer *mainLayer;

@property (nonatomic, strong) CAShapeLayer *blackCircleLayer;
@property (nonatomic, strong) CAShapeLayer *greenCircleLayer;
@property (nonatomic, strong) CAShapeLayer *orangeOutlineLayer;

@end

@implementation InteractiveView

- (instancetype)initWithFrame:(NSRect)frameRect
{
    self = [super initWithFrame:frameRect];
    
    if (self) {
        NSLog(@"w/frame");
        [self performInitWork];
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    
    if (self) {
        NSLog(@"w/coder");
        [self performInitWork];
    }
    
    return self;
}

- (void)viewDidMoveToWindow
{
    NSTrackingArea *trackingArea = [[NSTrackingArea alloc] initWithRect:NSMakeRect(50, 50, 100, 100)
                                                                options:(NSTrackingMouseEnteredAndExited | NSTrackingActiveInKeyWindow)
                                                                  owner:self
                                                               userInfo:nil];
    
    [self addTrackingArea:trackingArea];
}

- (void)performInitWork
{
    self.wantsLayer = YES;
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddEllipseInRect(path, NULL, NSMakeRect(25.0, 25.0, 150.0, 150.0));
    
    CGMutablePathRef hoverOutlinePath = CGPathCreateMutable();
    CGPathAddArc(hoverOutlinePath, NULL, 100.0, 100.0, 87.0, 0.0, M_PI*2, true);
    CGPathAddArc(hoverOutlinePath, NULL, 100.0, 100.0, 86.0, 0.0, M_PI*2, false);
    
    // Black circle
    self.blackCircleLayer = [CAShapeLayer layer];
    self.blackCircleLayer.frame = NSMakeRect(0, 0, 200, 200);
    self.blackCircleLayer.bounds = NSMakeRect(0, 0, 200, 200);
    self.blackCircleLayer.path = path;
    self.blackCircleLayer.fillColor = CGColorCreateGenericRGB(0.0, 0.0, 0.0, 1.0);
    self.blackCircleLayer.anchorPoint = CGPointMake(0.5, 0.5);
    [self.blackCircleLayer setNeedsDisplay];
    
    // Green circle
    self.greenCircleLayer = [CAShapeLayer layer];
    self.greenCircleLayer.frame = NSMakeRect(0, 0, 200, 200);
    self.greenCircleLayer.bounds = NSMakeRect(0, 0, 200, 200);
    self.greenCircleLayer.path = path;
    self.greenCircleLayer.fillColor = CGColorCreateGenericRGB(0.0, 1.0, 0.0, 1.0);
    self.greenCircleLayer.anchorPoint = CGPointMake(0.5, 0.5);
    [self.greenCircleLayer setNeedsDisplay];
    
    // Orange hover ring
    self.orangeOutlineLayer = [CAShapeLayer layer];
    self.orangeOutlineLayer.frame = NSMakeRect(0, 0, 200, 200);
    self.orangeOutlineLayer.bounds = NSMakeRect(0, 0, 200, 200);
    self.orangeOutlineLayer.path = hoverOutlinePath;
    self.orangeOutlineLayer.fillColor = CGColorCreateGenericRGB(1.00, 0.50, 0.00, 1.00);
    self.orangeOutlineLayer.strokeColor = CGColorCreateGenericRGB(1.00, 0.50, 0.00, 1.00);
    self.orangeOutlineLayer.strokeStart = 0.0;
    self.orangeOutlineLayer.strokeEnd = 1.0;
    self.orangeOutlineLayer.opacity = 0.0f;
    self.orangeOutlineLayer.anchorPoint = CGPointMake(0.5, 0.5);
    [self.orangeOutlineLayer setNeedsDisplay];
    
    self.mainLayer = [CALayer layer];
    [self.mainLayer addSublayer:self.greenCircleLayer];
    [self.mainLayer addSublayer:self.orangeOutlineLayer];
    [self.mainLayer addSublayer:self.blackCircleLayer];
    [self.mainLayer setNeedsDisplay];
    
    self.layer = self.mainLayer;
    
    CFRelease(path);
    CFRelease(hoverOutlinePath);
}

- (void)mouseDown:(NSEvent *)theEvent
{
    NSLog(@"mousedown");
    
    NSPoint mouseLocation = theEvent.locationInWindow;
    
    if ([self.blackCircleLayer hitTest:mouseLocation]) {
        [self.orangeOutlineLayer removeAnimationForKey:@"opacity"];
        [self.orangeOutlineLayer removeAnimationForKey:@"transform.scale"];
        
        CABasicAnimation *hoverOrangeFadeIn = [CABasicAnimation animationWithKeyPath:@"opacity"];
        hoverOrangeFadeIn.fromValue = [NSNumber numberWithFloat:1.0f];
        hoverOrangeFadeIn.toValue = [NSNumber numberWithFloat:0.0f];
        hoverOrangeFadeIn.duration = 0.10f;
        hoverOrangeFadeIn.fillMode = kCAFillModeForwards;
        hoverOrangeFadeIn.removedOnCompletion = NO;
        
        CABasicAnimation *hoverOrangeGrowShrink = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        hoverOrangeGrowShrink.fromValue = [NSNumber numberWithFloat:1.05f];
        hoverOrangeGrowShrink.toValue = [NSNumber numberWithFloat:1.0f];
        hoverOrangeGrowShrink.duration = 0.10f;
        hoverOrangeGrowShrink.autoreverses = YES;
        hoverOrangeGrowShrink.repeatCount = HUGE_VALF;
        
        CAAnimationGroup *orangeOutAnimations = [CAAnimationGroup animation];
        orangeOutAnimations.duration = 0.10f;
        orangeOutAnimations.animations = @[hoverOrangeFadeIn, hoverOrangeGrowShrink];
        orangeOutAnimations.fillMode = kCAFillModeForwards;
        orangeOutAnimations.removedOnCompletion = YES;
        orangeOutAnimations.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        
        [self.orangeOutlineLayer addAnimation:orangeOutAnimations forKey:@"orangeOut"];
        
        
        [self.greenCircleLayer removeAnimationForKey:@"greenOut"];
        
        CABasicAnimation *scaleAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        scaleAnimation.fromValue = [NSNumber numberWithFloat:1.0f];
        scaleAnimation.toValue = [NSNumber numberWithFloat:1.20f];
        scaleAnimation.duration = 0.30f;
        scaleAnimation.fillMode = kCAFillModeForwards;
        scaleAnimation.removedOnCompletion = NO;
        
        CABasicAnimation *colorAnimation = [CABasicAnimation animationWithKeyPath:@"fillColor"];
        colorAnimation.fromValue = (__bridge id _Nullable)(CGColorCreateGenericRGB(0.0, 0.0, 0.0, 1.0));
        colorAnimation.toValue = (__bridge id _Nullable)(CGColorCreateGenericRGB(0.0, 1.0, 0.0, 1.0));
        colorAnimation.duration = 0.15f;
        colorAnimation.fillMode = kCAFillModeForwards;
        colorAnimation.removedOnCompletion = NO;
        
        CAAnimationGroup *animations = [CAAnimationGroup animation];
        animations.duration = 0.30f;
        animations.animations = @[scaleAnimation, colorAnimation];
        animations.fillMode = kCAFillModeForwards;
        animations.removedOnCompletion = NO;
        animations.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
        
        [self.greenCircleLayer addAnimation:animations forKey:@"growGreen"];
        
    }
}

- (void)mouseEntered:(NSEvent *)theEvent
{
    NSLog(@"mouseEntered");
    
    NSPoint mouseLocation = theEvent.locationInWindow;
    
    if ([self.blackCircleLayer hitTest:mouseLocation]) {
        CABasicAnimation *hoverOrangeFadeIn = [CABasicAnimation animationWithKeyPath:@"opacity"];
        hoverOrangeFadeIn.fromValue = [NSNumber numberWithFloat:0.0f];
        hoverOrangeFadeIn.toValue = [NSNumber numberWithFloat:1.0f];
        hoverOrangeFadeIn.duration = 0.15f;
        hoverOrangeFadeIn.fillMode = kCAFillModeForwards;
        hoverOrangeFadeIn.removedOnCompletion = NO;
        
        CABasicAnimation *hoverOrangeGrowShrink = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        hoverOrangeGrowShrink.fromValue = [NSNumber numberWithFloat:1.0f];
        hoverOrangeGrowShrink.toValue = [NSNumber numberWithFloat:1.05f];
        hoverOrangeGrowShrink.duration = 0.75f;
        hoverOrangeGrowShrink.autoreverses = YES;
        hoverOrangeGrowShrink.repeatCount = HUGE_VALF;

        [self.orangeOutlineLayer addAnimation:hoverOrangeFadeIn forKey:@"opacity"];
        [self.orangeOutlineLayer addAnimation:hoverOrangeGrowShrink forKey:@"transform.scale"];
    }
}

- (void)mouseExited:(NSEvent *)theEvent
{
    NSPoint mouseLocation = theEvent.locationInWindow;
    
    if ([self.blackCircleLayer hitTest:mouseLocation] &&
        [self.greenCircleLayer animationForKey:@"growGreen"] != nil)
    {
        [self.greenCircleLayer removeAnimationForKey:@"growGreen"];
        
        CABasicAnimation *greenFadeOut = [CABasicAnimation animationWithKeyPath:@"opacity"];
        greenFadeOut.fromValue = [NSNumber numberWithFloat:1.20f];
        greenFadeOut.toValue = [NSNumber numberWithFloat:0.0f];
        greenFadeOut.duration = 0.5f;
        greenFadeOut.fillMode = kCAFillModeForwards;
        greenFadeOut.removedOnCompletion = NO;
        
        CABasicAnimation *greenGrowShrink = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        greenGrowShrink.fromValue = [NSNumber numberWithFloat:1.05f];
        greenGrowShrink.toValue = [NSNumber numberWithFloat:1.0f];
        greenGrowShrink.duration = 0.5f;
        greenGrowShrink.autoreverses = YES;
        greenGrowShrink.repeatCount = HUGE_VALF;
        
        CAAnimationGroup *greenOutAnimations = [CAAnimationGroup animation];
        greenOutAnimations.duration = 0.5f;
        greenOutAnimations.animations = @[greenFadeOut, greenGrowShrink];
        greenOutAnimations.fillMode = kCAFillModeForwards;
        greenOutAnimations.removedOnCompletion = NO;
        greenOutAnimations.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault];
        
        [self.greenCircleLayer addAnimation:greenOutAnimations forKey:@"greenOut"];
    } else if ([self.blackCircleLayer hitTest:mouseLocation] &&
        [self.orangeOutlineLayer animationForKey:@"opacity"] != nil &&
        [self.orangeOutlineLayer animationForKey:@"transform.scale"] != nil)
    {
        [self.orangeOutlineLayer removeAnimationForKey:@"opacity"];
        [self.orangeOutlineLayer removeAnimationForKey:@"transform.scale"];
        
        CABasicAnimation *hoverOrangeFadeIn = [CABasicAnimation animationWithKeyPath:@"opacity"];
        hoverOrangeFadeIn.fromValue = [NSNumber numberWithFloat:1.0f];
        hoverOrangeFadeIn.toValue = [NSNumber numberWithFloat:0.0f];
        hoverOrangeFadeIn.duration = 0.10f;
        hoverOrangeFadeIn.fillMode = kCAFillModeForwards;
        hoverOrangeFadeIn.removedOnCompletion = NO;
        
        CABasicAnimation *hoverOrangeGrowShrink = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        hoverOrangeGrowShrink.fromValue = [NSNumber numberWithFloat:1.05f];
        hoverOrangeGrowShrink.toValue = [NSNumber numberWithFloat:1.0f];
        hoverOrangeGrowShrink.duration = 0.10f;
        hoverOrangeGrowShrink.autoreverses = YES;
        hoverOrangeGrowShrink.repeatCount = HUGE_VALF;
        
        CAAnimationGroup *animations = [CAAnimationGroup animation];
        animations.duration = 0.10f;
        animations.animations = @[hoverOrangeFadeIn, hoverOrangeGrowShrink];
        animations.fillMode = kCAFillModeForwards;
        animations.removedOnCompletion = YES;
        animations.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        
        [self.orangeOutlineLayer addAnimation:animations forKey:@"orangeOut"];
    }
}

@end
